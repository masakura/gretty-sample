# Gretty でお手軽 Java Web アプリ開発

## 開発をはじめる
[AdoptOpenJDK](https://adoptopenjdk.net/) から OpenJDK 8 のインストーラーをダウンロードしてインストールしてください。

```
> git clone https://gitlab.com/masakura/gretty-sample.git
> cd gretty-sample
> gradlew appRun
```

起動したらブラウザーで `http://localhost:8080/` にアクセスします。
